// Package packagejson provides ...
package packagejson

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
)

// PackageJSON isdsd sdsd sd
type PackageJSON struct {
	Name            string            `json:"name,omitempty"`
	Description     string            `json:"description,omitempty"`
	DistTags        map[string]string `json:"dist-tags,omitempty"`
	Versions        []string          `json:"versions,omitempty"`
	Maintainer      string            `json:"maintainer,omitempty"`
	Time            map[string]string `json:"time,omitempty"`
	Homepage        string            `json:"homepage,omitempty"`
	Keywords        []string          `json:"keywords,omitempty"`
	Repository      map[string]string `json:"repository,omitempty"`
	Author          string            `json:"author,omitempty"`
	Bugs            map[string]string `json:"bugs,omitempty"`
	License         string            `json:"license,omitempty"`
	Version         string            `json:"version,omitempty"`
	Main            string            `json:"main,omitempty"`
	Typings         string            `json:"typings,omitempty"`
	Engines         map[string]string `json:"engines,omitempty"`
	DevDependencies map[string]string `json:"devDependencies,omitempty"`
	Dependencies    map[string]string `json:"dependencies,omitempty"`
	Scripts         map[string]string `json:"scripts,omitempty"`
	Dist            Dist              `json:"dist,omitempty"`
}

// Dist is the dist block in PackageJSON
type Dist struct {
	Integrity    string `json:"integrity,omitempty"`
	Shasum       string `json:"shasum,omitempty"`
	Tarball      string `json:"tarball,omitempty"`
	FileCount    int8   `json:"fileCount,omitempty"`
	UnpackedSize int32  `json:"unpackedSize,omitempty"`
	NpmSignature string `json:"npmSignature,omitempty"`
}

// LoadFromPath loads JSON to the PackageJSON struct
func LoadFromPath(path string) (PackageJSON, error) {
	var j PackageJSON
	dat, err := ioutil.ReadFile("info.json")
	if err != nil {
		return j, nil
	}
	err = json.Unmarshal(dat, &j)
	if err != nil {
		return j, err
	}

	return j, nil
}

// Unmarshal loads a bytes.Buffer and returns a PackageJSON
func Unmarshal(b bytes.Buffer) (PackageJSON, error) {
	var j PackageJSON

	err := json.Unmarshal(b.Bytes(), &j)
	if err != nil {
		return j, err
	}

	return j, nil
}

// Marshal marshales to a []byte
func Marshal(pjs PackageJSON) ([]byte, error) {
	var bytes []byte

	bytes, err := json.Marshal(pjs)
	if err != nil {
		return bytes, err
	}
	return bytes, nil
}

// MarshalIndent marshales to a []byte
func MarshalIndent(pjs PackageJSON) ([]byte, error) {
	var bytes []byte

	bytes, err := json.MarshalIndent(pjs, "", "  ")
	if err != nil {
		return bytes, err
	}
	return bytes, nil
}
