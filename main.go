// Package main provides ...
package main

import (
	"io/ioutil"
	"log"

	"github.com/danielcherubini/npm-go/npm"
	"github.com/danielcherubini/npm-go/packagejson"
)

func main() {
	info, err := npm.Info("express-vue")
	if err != nil {
		log.Fatal(err)
	}

	bytes, err := packagejson.MarshalIndent(info)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile("temp.json", bytes, 0644)
	if err != nil {
		log.Fatal(err)
	}
}
