// Package npm provides ...
package npm

import (
	"bytes"
	"os/exec"
	"regexp"
	"strings"

	"github.com/danielcherubini/npm-go/packagejson"
)

// Info gets package information from NPM
func Info(packageName string) (packagejson.PackageJSON, error) {
	var pjson packagejson.PackageJSON
	cmd := exec.Command("npm", "info", "--json", packageName)
	cmd.Stdin = strings.NewReader("some input")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		return pjson, err
	}

	pjson, err = packagejson.Unmarshal(out)
	if err != nil {
		return pjson, err
	}

	return pjson, nil
}

// GetTarballs gets the tars from npm
func GetTarballs(packageName string) ([]string, error) {
	var tarballs []string
	pjs, err := Info(packageName)
	if err != nil {
		return tarballs, err
	}

	var tarballRegxp = regexp.MustCompile(`\d.\d.\d.tgz`)

	for _, v := range pjs.Versions {
		str := tarballRegxp.ReplaceAllString(pjs.Dist.Tarball, v) + ".tgz"
		tarballs = append(tarballs, str)
	}

	return tarballs, nil
}
