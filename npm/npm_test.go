package npm

import (
	"strings"
	"testing"
)

func TestInfo(t *testing.T) {
	result, err := Info("express-vue")
	if err != nil {
		t.Fatal(err)
	}

	if result.Name != "express-vue" {
		t.Errorf("expecting 'express-vue', got %s", result.Name)
	}
}

func TestGetTarballs(t *testing.T) {
	tarballs, err := GetTarballs("express-vue")
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(tarballs[0], "express-vue") {
		t.Error("expecting first tarball in array to be express-vue")
	}

	tarballs, err = GetTarballs("express-vues")
	if err == nil {
		t.Fatal(err)
	}

}
